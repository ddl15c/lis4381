

# LIS 4381

## Demarco Lockhart

### Assignment 1 Requirements:

*Sub-Heading:*

1. ordered-list items
2. Screenshots of Applications
3. Bitbucket repository links

#### README.md file should include the following items:

* Screenshot of AMPPS Installation 
* Screenshot of Running Java Hello
* Screenshot of Running Android Studio - My First App
* Git Commands w/ Short Descriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a new local repository
2. git status - List the files you've changed and those you still need to add or commit:
3. git add - Add files to staging
4. git commit - Commit changes to head (but not yet to the remote repository):
5. git push - Send changes to the master branch of your remote repository:
6. git pull - Fetch and merge changes on the remote server to your working directory:
7. git log - lists version history for the current branch

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk.PNG)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ddl15c/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*A1 LIS 4381 Repository*
[A1 repository links](https://bitbucket.org/ddl15c/lis4381/src/master/ "A1 Repository Link")
