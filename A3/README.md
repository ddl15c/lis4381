

# LIS 4381

## Demarco Lockhart

### Assignment 3 Requirements:

*Create a Mobile Ticket Price Calculation App using Android Studio *

1. Screenshots of User Interfaces
2. Bitbucket Repo Links


#### README.md file should include the following items:
* Screenshot of ERD
* Links to mwb & sql files
* Screenshot of Running "My Event" App
* Bitbucket repo links

#### ERD Files:
*Link to MWB*:
[MWB File](https://bitbucket.org/ddl15c/lis4381/src/a69fcb0a386e/A3/?at=master "mwb file")

*Link to SQL*:
[SQL File](https://bitbucket.org/ddl15c/lis4381/src/a69fcb0a386e/A3/?at=master "sql file")

#### Assignment Screenshots:
*Screenshot of ERD*:
![ERD Screenshot](img/a3.png)

*Screenshot of User Interface 1*:

![User Interface 1 Screenshot](img/UI1.PNG)

*Screenshot of User Interface 1*:

![User Interface 2 Screenshot](img/UI2.PNG)

#### Tutorial Links:


*A1 LIS 4381 Repository*
[ LIS 4381 repository links](https://bitbucket.org/ddl15c/lis4381/src/master/ " LIS 4831 Repository Link")
