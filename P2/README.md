# LIS 4381

## Demarco Lockhart

### Project 2 Requirements:

*Create an online portfolio that illustrates the skills acquired while working through various projects in LIS4381.*

1. Screenshots of pages
2. Bitbucket Repo Links
3. Link to local LIS4381 Web App

#### README.md file should include the following items:

* Screenshot of petstore table
* Screenshot of Petstore Data Entry
* Screenshot of Error Page
* Screenshot of RSS Feed
* Link local LIS4381 Web App
* Bitbucket Repo Links


#### Assignment Screenshots:

*Screenshot of Main Page*:

![Main Page Screenshot](images/index.PNG)

*Screenshot of Petstore Tables*:

![Failed Validation Screenshot](images/table.PNG)

*Screenshot of Petstore Data*:

![Failed Validation Screenshot](images/data.PNG)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](images/error.PNG)

*Screenshot of RSS Feed*:

![Failed Validation Screenshot](images/rssfeed.PNG)




#### Tutorial Link and Assignment Link:

*A4 lis4381 Repository:*
[A4 Repository Link](https://bitbucket.org/ddl15c/lis4381/src/master/ "lis4381 Repository")
