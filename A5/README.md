# LIS 4381

## Demarco Lockhart

### Assignment 5 Requirements:

*Create an online portfolio that illustrates the skills acquired while working through various projects in LIS4381.*

1. Screenshots of pages
2. Bitbucket Repo Links
3. Link to local LIS4381 Web App

#### README.md file should include the following items:

* Screenshot of petstore table
* Screenshot of Error Page
* Link local LIS4381 Web App
* Bitbucket Repo Links


#### Assignment Screenshots:

*Screenshot of Main Page*:

![Main Page Screenshot](img/table.PNG)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/error.PNG)




#### Tutorial Link and Assignment Link:

*Links to the files:*
[Local LIS4381 Web App](https://localhost/repos/lis4381/index.php)

*Assignment: Class repository:*
[A4 My Class Repository Link](https://bitbucket.org/ddl15c/lis4381/ "Class Repository")