# LIS4381

## Demarco Lockhart

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    * Installed AMPPS
    * Installed JDK and Java SE
    * Installed Android Studio and created My First App
    * Provided screenshots of all installations
    * Created Bitbucket repo
    * Completed Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command descriptions



2. [A2 README.md](A2/README.md "My A2 README.md file")
    * Create First Inteface for Recipes app
    * Create Second Interface for Recipes app
    * Push all deliverables to bitbucket




3. [A3 README.md](A3/README.md "My A3 README.md file")
     * Created ERD Model 
     * Forward Egineer ERD Model
     * Provided link to mwb & sql files
     * Provided Screenshots of ERD Model
     * Created Ticket Price Calculation (My Event) App
     * Provided Screenshots of My Event App




4. [P1 README.md](P1/README.md "My P1 README.md file")
    * Create First Inteface for Business Card app
    * Create Second Interface for Business Card app
    * Push all deliverables to bitbucket




 5. [A4 README.md](A4/README.md "My A4 README.md file")
    * Provided screenshot of Main Page
    * Provided screenshot of Failed Validation
    * Provided screenshot of Passed Validation
    * Provided link local LIS4381 Web App
    * Push all deliverables to bitbucket




6. [A5 READMe.md](A5/README.md "My A5 README.md file")
    * Provided screenshot of Table
    * Provided screenshot of Error Page
    * Provided link local LIS4381 Web App
    * Push all deliverables to bitbucket



7. [P2 READMe.md](P2/README.md "My P2 README.md file")
    * Screenshot of Home Page
    * Provided screenshot of Table
    * Provided screenshot of Data Entry
    * Provided screenshot of Error Page
    * Provided Screenshot of RSS Feed
    * Provided link local LIS4381 Web App
    * Push all deliverables to bitbucket
