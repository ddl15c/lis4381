

# LIS 4381

## Demarco Lockhart

### Assignment 2 Requirements:

*Create a Mobile Recipe App using Android Studio *

1. Screenshots of User Interfaces
2. Bitbucket Repo Links


#### README.md file should include the following items:

* Screenshot of Running First User Interface
* Screenshot of Running Second User Interface
* Bitbucket repo links


#### Assignment Screenshots:

*Screenshot of User Interface 1*:

![User Interface 1 Screenshot](img/UI1.PNG)

*Screenshot of User Interface 1*:

![User Interface 2 Screenshot](img/UI2.PNG)

#### Tutorial Links:


*A1 LIS 4381 Repository*
[ LIS 4381 repository links](https://bitbucket.org/ddl15c/lis4381/src/master/ " LIS 4831 Repository Link")
